import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    searchQuery: '',
    shipments: []
  },
  getters: {
    getSearchQuery: ({ searchQuery }) => searchQuery,
    getShipments: ({ shipments }) => shipments,
    getShipmentById: ({ shipments }) => (id) => {
      return shipments.find(shipment => shipment.id === id);
    }
  },
  mutations: {
    SET_SEARCH_QUERY(state, query) {
      state.searchQuery = query
    },
    SET_SHIPMENTS(state, data) {
      state.shipments = data
    },
    UPDATE_SHIPMENT_BY_ID(state, { id, data }) {
      const newData = [...state.shipments]
      const index = state.shipments.findIndex(shipment => shipment.id === id)
      newData[index] = data
      state.shipments = [...newData]
    }
  },
  actions: {
    initShipments({ state, commit }) {
      const savedShipments = localStorage.getItem('shipments')
      if (savedShipments && savedShipments.length) {
        commit('SET_SHIPMENTS', JSON.parse(savedShipments))
      }

      return state.shipments.length
    },

    loadShipments({ commit }) {
      return axios
        .get('/shipments.json')
        .then((res) => {
          commit('SET_SHIPMENTS', res.data)
        })
        .catch(error => console.log(error))
    },

    saveShipments({ state }) {
      localStorage.setItem('shipments', JSON.stringify(state.shipments))
    }
  }
})
